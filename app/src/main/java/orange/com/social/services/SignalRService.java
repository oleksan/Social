package orange.com.social.services;

import android.app.Service;
import android.content.Intent;

import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonElement;

import java.util.concurrent.ExecutionException;

import microsoft.aspnet.signalr.client.Action;
import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.Credentials;
import microsoft.aspnet.signalr.client.Platform;
import microsoft.aspnet.signalr.client.SignalRFuture;
import microsoft.aspnet.signalr.client.StateChangedCallback;
import microsoft.aspnet.signalr.client.http.Request;
import microsoft.aspnet.signalr.client.http.android.AndroidPlatformComponent;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler1;
import microsoft.aspnet.signalr.client.transport.ClientTransport;
import microsoft.aspnet.signalr.client.transport.ServerSentEventsTransport;
import orange.com.social.models.FriendNotificationViewModel;
import orange.com.social.models.Message;
import orange.com.social.activities.MessangerActivity;

import static orange.com.social.activities.MainActivity.token;


public class SignalRService extends Service {
    private HubConnection mHubConnection;
    private HubProxy mHubProxy;
    private Handler mHandler; // to display Toast message
    private final IBinder mBinder = new LocalBinder(); // Binder given to clients
    private Message mes;

    public SignalRService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mHandler = new Handler(Looper.getMainLooper());

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int result = super.onStartCommand(intent, flags, startId);
        Toast.makeText(this, "onStartCommand", Toast.LENGTH_SHORT).show();
        startSignalR();
        return result;
    }

    @Override
    public void onDestroy() {
        mHubConnection.stop();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Return the communication channel to the service.
        startSignalR();
        return mBinder;
    }


    public class LocalBinder extends Binder {
        public SignalRService getService() {
            // Return this instance of SignalRService so clients can call public methods
            return SignalRService.this;
        }
    }

    private void startSignalR() {
        Platform.loadPlatformComponent(new AndroidPlatformComponent());
        String serverUrl = "http://10.0.3.2:49342/SignalR/Hubs";
        mHubConnection = new HubConnection(serverUrl);
        String SERVER_HUB_CHAT = "notificationHub";
        Credentials credentials = new Credentials() { // add Authorization header
            @Override
            public void prepareRequest(Request request) {
                request.addHeader("Authorization", "bearer " + token);

            }
        };
        mHubConnection.setCredentials(credentials);
        mHubProxy = mHubConnection.createHubProxy(SERVER_HUB_CHAT);
        ClientTransport clientTransport = new ServerSentEventsTransport(mHubConnection.getLogger());
        SignalRFuture<Void> signalRFuture = mHubConnection.start(clientTransport);

        try {
            signalRFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e("SimpleSignalR", e.toString());
            return;
        }

        String CLIENT_METHOD_NEW_MESSAGE_FROM_PRIVATE_CHAT = "newMessageFromPrivateChat"; // method name on server
        mHubProxy.on(CLIENT_METHOD_NEW_MESSAGE_FROM_PRIVATE_CHAT,
                new SubscriptionHandler1<Message>() {
                    @Override
                    public void run(final Message msg) { // result from hub
                        MessangerActivity.updateNewMessage(msg);
                        // display Toast message
                        Toast.makeText(getApplicationContext(), msg.getMessageText(), Toast.LENGTH_SHORT).show();
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "You new message: " +
                                        msg.getMessageText(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
                , Message.class);

        String CLIENT_METHOD_NEW_MESSAGE_FROM_GROUP_CHAT = "newMessageFromGroupChat";
        mHubProxy.on(CLIENT_METHOD_NEW_MESSAGE_FROM_GROUP_CHAT,
                new SubscriptionHandler1<Message>() {
                    @Override
                    public void run(final Message msg) {
                        MessangerActivity.updateNewMessage(msg); // add new message to recyclerView
                        // display Toast message
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "You new message from group: " +
                                        msg.getMessageText(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
                , Message.class);

        String CLIENT_METHOD_CONFIRM_FRIEND_NOTIFICATION = "confirmFriendNotification";
        mHubProxy.on(CLIENT_METHOD_CONFIRM_FRIEND_NOTIFICATION,
                new SubscriptionHandler1<FriendNotificationViewModel>() {
                    @Override
                    public void run(final FriendNotificationViewModel msg) {
                        // display Toast message
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), msg.getSender().getFirstName() + " "
                                        + msg.getSender().getLastName() + " added you to friends", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
                , FriendNotificationViewModel.class);

        String CLIENT_METHOD_SUBSCRIBE_NOTIFICATION = "subscribeNotification";
        mHubProxy.on(CLIENT_METHOD_SUBSCRIBE_NOTIFICATION,
                new SubscriptionHandler1<FriendNotificationViewModel>() {
                    @Override
                    public void run(final FriendNotificationViewModel msg) {
                        // display Toast message
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), msg.getSender().getLastName() + " "
                                        + msg.getSender().getLastName() + " want to friend with you", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
                , FriendNotificationViewModel.class);

        mHubProxy.subscribe("newMessageFromPrivateChat").addReceivedHandler(new Action<JsonElement[]>() {
            @Override
            public void run(JsonElement[] jsonElements) throws Exception {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(), "Message from: ", Toast.LENGTH_SHORT).show();
                    }
                });
                Toast.makeText(SignalRService.this, "Message from: " + jsonElements.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        mHubConnection.stateChanged(new StateChangedCallback() {
            @Override
            public void stateChanged(ConnectionState connectionState, ConnectionState connectionState2) {
                Log.i("SignalR", connectionState.name() + "->" + connectionState2.name());
                Toast.makeText(SignalRService.this, connectionState.name() + "->" + connectionState2.name(), Toast.LENGTH_SHORT).show();
            }
        });

        mHubConnection.closed(new Runnable() {
            @Override
            public void run() {
                Log.i("SignalR", "Closed");
                Toast.makeText(SignalRService.this, "Closed", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
