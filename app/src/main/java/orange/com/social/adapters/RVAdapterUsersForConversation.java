package orange.com.social.adapters;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import orange.com.social.activities.ChatActivity;
import orange.com.social.activities.ProfileActivity;
import orange.com.social.R;
import orange.com.social.models.User;


public class RVAdapterUsersForConversation extends RecyclerView.Adapter<RVAdapterUsersForConversation.UserForConversationViewHolder> {

    public class UserForConversationViewHolder extends RecyclerView.ViewHolder implements
            View.OnCreateContextMenuListener {
        CardView cv;
        TextView tvUserFullName;
        TextView tvUserYears;
        ImageView userPhoto;
        CheckBox cbSelect;

        UserForConversationViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv_user);
            tvUserFullName = (TextView) itemView.findViewById(R.id.user_fullname);
            tvUserYears = (TextView) itemView.findViewById(R.id.user_years);
            userPhoto = (ImageView) itemView.findViewById(R.id.user_photo);
            cbSelect = (CheckBox) itemView.findViewById(R.id.cbSelect);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            MenuItem sendMessage = menu.add(0, v.getId(), 1, "Send message");//(groupId, itemId, order, title)
            MenuItem viewProfile = menu.add(0, v.getId(), 2, "View profile");
            sendMessage.setOnMenuItemClickListener(onClickMenu);
            viewProfile.setOnMenuItemClickListener(onClickMenu);
        }

        private final MenuItem.OnMenuItemClickListener onClickMenu = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getOrder()) {
                    case 1:
                        Intent intentChat = new Intent(itemView.getContext(), ChatActivity.class);
                        intentChat.putExtra("friend id", users.get(getAdapterPosition()).getId());
                        itemView.getContext().startActivity(intentChat);
                        break;
                    case 2:
                        Intent intentProfile = new Intent(itemView.getContext(), ProfileActivity.class);
                        intentProfile.putExtra("friend id", users.get(getAdapterPosition()).getId());
                        intentProfile.putExtra("friend first name", users.get(getAdapterPosition()).getFirstName());
                        intentProfile.putExtra("friend last name", users.get(getAdapterPosition()).getLastName());
                        intentProfile.putExtra("friend date of birth", users.get(getAdapterPosition()).getDateOfBirth());
                        intentProfile.putExtra("friend gender", users.get(getAdapterPosition()).getGender());
                        itemView.getContext().startActivity(intentProfile);
                        break;
                }
                return true;
            }

        };

    }

    private List<User> users;

    public RVAdapterUsersForConversation(List<User> users) {
        this.users = users;
    }

    @Override
    public UserForConversationViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_for_conversation, viewGroup, false);
        UserForConversationViewHolder uvh = new UserForConversationViewHolder(v);
        return uvh;

    }

    @Override
    public void onBindViewHolder(final UserForConversationViewHolder userViewHolder, int position) {
        userViewHolder.tvUserFullName.setText(users.get(position).getFirstName() + " " + users.get(position).getLastName());
        userViewHolder.tvUserYears.setText(RVAdapterAllUsers.countUserYears(users.get(position).getDateOfBirth()) + " years");
        if (users.get(position).getGender().equals("male")) {
            userViewHolder.userPhoto.setImageResource(R.drawable.ic_man);
        } else if (users.get(position).getGender().equals("female")) {
            userViewHolder.userPhoto.setImageResource(R.drawable.ic_woman);
        }

        userViewHolder.cbSelect.setOnCheckedChangeListener(null);////
        userViewHolder.cbSelect.setSelected(users.get(position).isSelected());
        userViewHolder.cbSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                users.get(userViewHolder.getAdapterPosition()).setSelected(isChecked);
            }
        });

    }

    public List<String> acceptUsersId() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).isSelected()) {
                list.add(users.get(i).getId());
            }
        }
        return list;
    }

    @Override
    public int getItemCount() {
        if (users == null)
            return 0;
        return users.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}