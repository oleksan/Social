package orange.com.social.adapters;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import orange.com.social.api.App;
import orange.com.social.models.ConversationModel;
import orange.com.social.activities.MessangerActivity;
import orange.com.social.R;
import orange.com.social.models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static orange.com.social.activities.ChatActivity.myId;
import static orange.com.social.activities.MainActivity.token;


public class RVAdapterConversations extends RecyclerView.Adapter<RVAdapterConversations.ConversationViewHolder> {


    public class ConversationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnCreateContextMenuListener {
        CardView cv;
        TextView tvConversationName;
        ImageView conversationPhoto;

        ConversationViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv_conversation);
            tvConversationName = (TextView) itemView.findViewById(R.id.conversation_name);
            conversationPhoto = (ImageView) itemView.findViewById(R.id.conversation_photo);
            itemView.setOnClickListener(this);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intentMessangerId = new Intent(v.getContext(), MessangerActivity.class);
            intentMessangerId.putExtra("conversation id", conversations.get(getAdapterPosition()).getId());
            intentMessangerId.putExtra("conversation name", conversations.get(getAdapterPosition()).getName());
            v.getContext().startActivity(intentMessangerId);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            MenuItem sendMessage = menu.add(0, v.getId(), 1, "Send message");//(groupId, itemId, order, title)
            MenuItem leaveConversation = menu.add(0, v.getId(), 2, "Leave conversation");
            sendMessage.setOnMenuItemClickListener(onClickMenu);
            leaveConversation.setOnMenuItemClickListener(onClickMenu);
        }

        private final MenuItem.OnMenuItemClickListener onClickMenu = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getOrder()) {
                    case 1:
                        Intent intentMessangerId = new Intent(itemView.getContext(), MessangerActivity.class);
                        intentMessangerId.putExtra("conversation id", conversations.get(getAdapterPosition()).getId());
                        itemView.getContext().startActivity(intentMessangerId);
                        break;
                    case 2:
                        postLeaveConversation();
                        break;
                }
                return true;
            }
        };

        private void postLeaveConversation() {

            int conversationId;
            conversationId = conversations.get(getAdapterPosition()).getId();
            Toast.makeText(cv.getContext(), conversationId + " " + getAdapterPosition(), Toast.LENGTH_SHORT).show();
            Call<ResponseBody> call = App.getOrangeApi().leaveConversation("bearer " + token,
                    "application/json",
                    conversationId);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        try {
                            Toast.makeText(itemView.getContext(),
                                    "You leave conversation " + response.body().string(), Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        deleteRowConversation(getAdapterPosition());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("Error", t.getMessage());
                }
            });
        }

    }

    private List<ConversationModel> conversations;

    public RVAdapterConversations(List<ConversationModel> allConversations) {
        this.conversations = allConversations;
    }

    @Override
    public RVAdapterConversations.ConversationViewHolder onCreateViewHolder(ViewGroup viewGroup,
                                                                            int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_conversations, viewGroup, false);
        RVAdapterConversations.ConversationViewHolder cvh =
                new RVAdapterConversations.ConversationViewHolder(v);
        return cvh;
    }

    @Override
    public void onBindViewHolder(RVAdapterConversations.ConversationViewHolder conversationViewHolder,
                                 int position) {
        conversationViewHolder.tvConversationName.setText(conversations.get(position).getName());
        if (conversations.get(position).isGroupChat()) {
            conversationViewHolder.conversationPhoto.setImageResource(R.drawable.ic_team);
        } else {
            for (User u : conversations.get(position).getUsers()) {
                if (!u.getId().equals(myId)) {
                    if (u.getGender().equals("male")) {
                        conversationViewHolder.conversationPhoto.setImageResource(R.drawable.ic_man);
                    } else if (u.getGender().equals("female")) {
                        conversationViewHolder.conversationPhoto.setImageResource(R.drawable.ic_woman);
                    }
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (conversations == null)
            return 0;
        return conversations.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void deleteRowConversation(int position) {
        conversations.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, conversations.size());
    }

}
