package orange.com.social.adapters;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import orange.com.social.api.App;
import orange.com.social.activities.MessangerActivity;
import orange.com.social.activities.ProfileActivity;
import orange.com.social.R;
import orange.com.social.models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static orange.com.social.activities.MainActivity.token;

public class RVAdapterAllUsers extends RecyclerView.Adapter<RVAdapterAllUsers.UserViewHolder> {


    public class UserViewHolder extends RecyclerView.ViewHolder implements
            View.OnCreateContextMenuListener {
        CardView cv;
        TextView tvUserFullName;
        TextView tvUserYears;
        ImageView userPhoto;
        Button btnAddUser;

        UserViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv_user);
            tvUserFullName = (TextView) itemView.findViewById(R.id.user_fullname);
            tvUserYears = (TextView) itemView.findViewById(R.id.user_years);
            userPhoto = (ImageView) itemView.findViewById(R.id.user_photo);
            btnAddUser = (Button) itemView.findViewById(R.id.btn_add_user);
            itemView.setOnCreateContextMenuListener(this);
            btnAddUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    postSubscribe();
                }
            });
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            MenuItem sendMessage = menu.add(0, v.getId(), 1, "Send message");//(groupId, itemId, order, title)
            MenuItem viewProfile = menu.add(0, v.getId(), 2, "View profile");
            sendMessage.setOnMenuItemClickListener(onClickMenu);
            viewProfile.setOnMenuItemClickListener(onClickMenu);
        }

        private final MenuItem.OnMenuItemClickListener onClickMenu = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getOrder()) {
                    case 1:
                        Intent intentMessanger = new Intent(itemView.getContext(), MessangerActivity.class);
                        intentMessanger.putExtra("friend id", users.get(getAdapterPosition()).getId());//
                        itemView.getContext().startActivity(intentMessanger);
                        break;
                    case 2:
                        Intent intentProfile = new Intent(itemView.getContext(), ProfileActivity.class);
                        intentProfile.putExtra("friend id", users.get(getAdapterPosition()).getId());
                        intentProfile.putExtra("friend first name", users.get(getAdapterPosition()).getFirstName());
                        intentProfile.putExtra("friend last name", users.get(getAdapterPosition()).getLastName());
                        intentProfile.putExtra("friend date of birth", users.get(getAdapterPosition()).getDateOfBirth());
                        intentProfile.putExtra("friend gender", users.get(getAdapterPosition()).getGender());
                        itemView.getContext().startActivity(intentProfile);
                        break;
                }
                return true;
            }

        };

        private void postSubscribe() {
            String recieverId = users.get(getAdapterPosition()).getId();
            Call<Void> call = App.getOrangeApi().subscribe("bearer " + token,
                    "application/json",
                    recieverId);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(cv.getContext(), "You subscribed on "
                                + users.get(getAdapterPosition()).getFirstName() + " "
                                + users.get(getAdapterPosition()).getLastName(), Toast.LENGTH_SHORT).show();
                        btnAddUser.setEnabled(false);
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Log.d("Error", t.getMessage());
                }
            });

        }

    }

    private List<User> users;

    public RVAdapterAllUsers(List<User> users) {
        this.users = users;
    }


    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_allusers, viewGroup, false);
        UserViewHolder uvh = new UserViewHolder(v);
        return uvh;
    }

    @Override
    public void onBindViewHolder(UserViewHolder userViewHolder, int position) {
        userViewHolder.tvUserFullName.setText(users.get(position).getFirstName() + " " + users.get(position).getLastName());
        userViewHolder.tvUserYears.setText(countUserYears(users.get(position).getDateOfBirth()) + " years");
        if (users.get(position).getGender().equals("male")) {
            userViewHolder.userPhoto.setImageResource(R.drawable.ic_man);
        } else if (users.get(position).getGender().equals("female")) {
            userViewHolder.userPhoto.setImageResource(R.drawable.ic_woman);
        }
        if (users.get(position).getTypeOfFriendship() != 3) { // unable for friens follows subscribers
            userViewHolder.btnAddUser.setEnabled(false);
        }
    }

    public static Integer countUserYears(String userBirthday) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"); // date format from server
        String currentDateandTime = sdf.format(Calendar.getInstance().getTime());
        String[] ar1 = userBirthday.split("-");
        String[] ar2 = currentDateandTime.split("-");
        Integer result = Integer.valueOf(ar2[0]) - Integer.valueOf(ar1[0]);
        if (Integer.valueOf(ar2[1]) - Integer.valueOf(ar1[1]) > 0) {
            return result;
        } else if (Integer.valueOf(ar2[1]) - Integer.valueOf(ar1[1]) < 0) {
            return result - 1;
        } else {
            String[] array1 = ar1[2].split("T");
            String[] array2 = ar2[2].split("T");
            if (Integer.valueOf(array2[0]) - Integer.valueOf(array1[0]) > 0) {
                return result;

            } else if (Integer.valueOf(array2[0]) - Integer.valueOf(array1[0]) < 0) {
                return result - 1;
            } else {
                return result;
            }
        }
    }

    @Override
    public int getItemCount() {
        if (users == null)
            return 0;
        return users.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
