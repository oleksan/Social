package orange.com.social.adapters;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import orange.com.social.api.App;
import orange.com.social.models.Message;
import orange.com.social.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static orange.com.social.activities.ChatActivity.myId;
import static orange.com.social.activities.MainActivity.token;

public class RVAdapterMessanger extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    public class MessageViewHolderMy extends RecyclerView.ViewHolder implements
            View.OnCreateContextMenuListener {
        TextView tvMessageUserFullNameMy;
        TextView tvMessageDateMy;
        TextView tvMessageTextMy;

        MessageViewHolderMy(View itemView) {
            super(itemView);
            tvMessageUserFullNameMy = (TextView) itemView.findViewById(R.id.message_user_fullname_my);
            tvMessageDateMy = (TextView) itemView.findViewById(R.id.message_date_my);
            tvMessageTextMy = (TextView) itemView.findViewById(R.id.message_text_my);
            itemView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            MenuItem deleteMessage = menu.add(0, v.getId(), 1, "Delete message");//(groupId, itemId, order, title)
            deleteMessage.setOnMenuItemClickListener(onClickMenu);
        }

        private final MenuItem.OnMenuItemClickListener onClickMenu = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getOrder()) {
                    case 1:
                        int messageId = messages.get(getAdapterPosition()).getId();
                        postDeleteMessage(messageId);
                        break;
                }
                return true;
            }
        };

        private void postDeleteMessage(int messageIdForDelete) {
            Call<Void> call = App.getOrangeApi().deleteMessage("bearer " + token,
                    "application/json",
                    messageIdForDelete);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(itemView.getContext(), "Message was deleted", Toast.LENGTH_SHORT).show();
                        deleteMyMessage(getAdapterPosition());
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Log.d("Error", t.getMessage());
                }
            });
        }

    }

    public class MessageViewHolderNotMy extends RecyclerView.ViewHolder {
        TextView tvMessageUserFullName;
        TextView tvMessageDate;
        TextView tvMessageText;

        MessageViewHolderNotMy(View itemView) {
            super(itemView);
            tvMessageDate = (TextView) itemView.findViewById(R.id.message_date_not_my);
            tvMessageText = (TextView) itemView.findViewById(R.id.message_text_not_my);

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (messages.get(position).getSenderId().equals(myId))

        {
            return 0;
        } else {
            return 1;
        }
    }

    private List<Message> messages;

    public RVAdapterMessanger(List<Message> messages) {
        this.messages = messages;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View v;
        RecyclerView.ViewHolder mvh;
        switch (viewType) {
            case 0:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_message_my, viewGroup, false);
                mvh = new RVAdapterMessanger.MessageViewHolderMy(v);
                break;
            default:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_messsage_not_my, viewGroup, false);
                mvh = new RVAdapterMessanger.MessageViewHolderNotMy(v);
                break;
        }
        return mvh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        switch (this.getItemViewType(position)) {
            case 0:
                MessageViewHolderMy messageViewHolderMy = (MessageViewHolderMy) holder;

                messageViewHolderMy.tvMessageUserFullNameMy.setText("Me: ");
                if (messages.get(position).getDateOfSend() == null) {     //set up current date and time for My message
                    messageViewHolderMy.tvMessageDateMy.setText(setUpCurrentTimeOnMyMessage());
                } else {
                    messageViewHolderMy.tvMessageDateMy.setText(messages.get(position).getDateOfSend().substring(0, 19));
                }
                messageViewHolderMy.tvMessageTextMy.setText(messages.get(position).getMessageText());
                break;
            case 1:
                MessageViewHolderNotMy messageViewHolderNotMy = (MessageViewHolderNotMy) holder;
               /* messageViewHolderNotMy.tvMessageUserFullName.setText(messages.get(position).getSender().getLastName()
                        + " " + messages.get(position).getSender().getFirstName());*/
                messageViewHolderNotMy.tvMessageDate.setText(messages.get(position).getDateOfSend().substring(0, 19));
                messageViewHolderNotMy.tvMessageText.setText(messages.get(position).getMessageText());
                break;
        }
    }

    @Override
    public int getItemCount() {
        if (messages == null)
            return 0;
        return messages.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void deleteMyMessage(int position) {
        messages.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, messages.size());
    }

    public void addNewMessage(int position, Message msg) {
        messages.add(position, msg);
        notifyItemInserted(position);
        notifyItemRangeChanged(position, messages.size());
    }

    public void addNewMessageMy(int position, Message msg) {
        messages.add(position, msg);
        notifyItemInserted(position);
        notifyItemRangeChanged(position, messages.size());
    }

    private String setUpCurrentTimeOnMyMessage() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String currentDateAndTime = sdf.format(Calendar.getInstance().getTime());
        return currentDateAndTime;
    }
}