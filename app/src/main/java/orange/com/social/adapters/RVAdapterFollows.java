package orange.com.social.adapters;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import orange.com.social.api.App;
import orange.com.social.activities.ConversationsActivity;
import orange.com.social.activities.ProfileActivity;
import orange.com.social.R;
import orange.com.social.models.User;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static orange.com.social.activities.MainActivity.token;

public class RVAdapterFollows extends RecyclerView.Adapter<RVAdapterFollows.UserViewHolder> {


    public class UserViewHolder extends RecyclerView.ViewHolder implements
            View.OnCreateContextMenuListener {
        CardView cv;
        TextView tvFollowsFullName;
        TextView tvFollowsYears;
        ImageView followPhoto;
        Button btnUnsubscribe;

        UserViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.cv_follows);
            tvFollowsFullName = (TextView) itemView.findViewById(R.id.follow_fullname);
            tvFollowsYears = (TextView) itemView.findViewById(R.id.follow_years);
            followPhoto = (ImageView) itemView.findViewById(R.id.follow_photo);
            btnUnsubscribe = (Button) itemView.findViewById(R.id.btn_unsubscribe);
            itemView.setOnCreateContextMenuListener(this);

            btnUnsubscribe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    postUnsubscribe();
                }
            });
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            MenuItem sendMessage = menu.add(0, v.getId(), 1, "Send message");//(groupId, itemId, order, title)
            MenuItem viewProfile = menu.add(0, v.getId(), 2, "View profile");
            sendMessage.setOnMenuItemClickListener(onClickMenu);
            viewProfile.setOnMenuItemClickListener(onClickMenu);
        }

        private final MenuItem.OnMenuItemClickListener onClickMenu = new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getOrder()) {
                    case 1:
                        Intent intentConversation = new Intent(itemView.getContext(), ConversationsActivity.class);
                        itemView.getContext().startActivity(intentConversation);
                        break;
                    case 2:
                        Intent intentProfile = new Intent(itemView.getContext(), ProfileActivity.class);
                        intentProfile.putExtra("friend id", users.get(getAdapterPosition()).getId());
                        intentProfile.putExtra("friend first name", users.get(getAdapterPosition()).getFirstName());
                        intentProfile.putExtra("friend last name", users.get(getAdapterPosition()).getLastName());
                        intentProfile.putExtra("friend date of birth", users.get(getAdapterPosition()).getDateOfBirth());
                        intentProfile.putExtra("friend gender", users.get(getAdapterPosition()).getGender());
                        itemView.getContext().startActivity(intentProfile);
                        break;
                }
                return true;
            }
        };

        private void postUnsubscribe() {
            String reciverId = users.get(getAdapterPosition()).getId();

            Call<Void> call = App.getOrangeApi().unsubscribe("bearer " + token, "application/json", reciverId);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(cv.getContext(), "You unsubscribed of " + users.get(getAdapterPosition()).getFirstName() + " "
                                + users.get(getAdapterPosition()).getLastName(), Toast.LENGTH_SHORT).show();
                        deleteRow(getAdapterPosition()); //after delete getAdapterPosition() == -1
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    Log.d("Error", t.getMessage());
                }
            });
        }

    }

    private List<User> users;

    public RVAdapterFollows(List<User> users) {
        this.users = users;
    }


    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_card_follows, viewGroup, false);
        UserViewHolder uvh = new UserViewHolder(v);
        return uvh;

    }

    @Override
    public void onBindViewHolder(UserViewHolder userViewHolder, int position) {
        userViewHolder.tvFollowsFullName.setText(users.get(position).getFirstName() + " " + users.get(position).getLastName());
        userViewHolder.tvFollowsYears.setText(RVAdapterAllUsers.countUserYears(users.get(position).getDateOfBirth()) + " years");
        if (users.get(position).getGender().equals("male")) {
            userViewHolder.followPhoto.setImageResource(R.drawable.ic_man);
        } else if (users.get(position).getGender().equals("female")) {
            userViewHolder.followPhoto.setImageResource(R.drawable.ic_woman);
        }
    }

    @Override
    public int getItemCount() {
        if (users == null)
            return 0;
        return users.size();

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private void deleteRow(int position) { //removes the row of RV
        users.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, users.size());

    }


}
