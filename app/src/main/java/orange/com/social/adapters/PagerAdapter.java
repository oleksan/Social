package orange.com.social.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import orange.com.social.fragments.TabFragmentFollows;
import orange.com.social.fragments.TabFragmentFriends;
import orange.com.social.fragments.TabFragmentSubscribers;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                TabFragmentFriends tab1 = new TabFragmentFriends();
                return tab1;
            case 1:
                TabFragmentSubscribers tab2 = new TabFragmentSubscribers();
                return tab2;
            case 2:
                TabFragmentFollows tab3 = new TabFragmentFollows();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
