package orange.com.social.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class User {

    @SerializedName("Id")
    @Expose
    private String id;

    @SerializedName("FirstName")
    @Expose
    private String firstName;

    @SerializedName("LastName")
    @Expose
    private String lastName;

    @SerializedName("DateOfBirth")
    @Expose
    private String dateOfBirth;

    @SerializedName("Gender")
    @Expose
    private int gender;

    @SerializedName("AvatarUrl")
    @Expose
    private String avatarUrl;

    @SerializedName("Friendship")
    @Expose
    private int typeOfFriendship;

    private boolean isSelected; // for RVAdapterForConversation for choose conversation users

    public User(String id, String firstName, String lastName, String dateOfBirth, int gender, String avatarUrl, int typeOfFriendship) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.avatarUrl = avatarUrl;
        this.typeOfFriendship = typeOfFriendship;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        if (gender == 0) {
            return "male";
        } else {
            return "female";
        }
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public int getTypeOfFriendship() {
        return typeOfFriendship;
    }

    public void setTypeOfFriendship(int typeOfFriendship) {
        this.typeOfFriendship = typeOfFriendship;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
