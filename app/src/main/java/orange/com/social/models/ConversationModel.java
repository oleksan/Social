package orange.com.social.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ConversationModel {

    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("Messages")
    @Expose
    private ArrayList<Message> messages;

    @SerializedName("Users")
    @Expose
    private ArrayList<User> users;

    @SerializedName("GroupChat")
    @Expose
    private boolean groupChat;

    @SerializedName("Id")
    @Expose
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<User> users) {
        this.users = users;
    }

    public boolean isGroupChat() {
        return groupChat;
    }

    public void setGroupChat(boolean groupChat) {
        this.groupChat = groupChat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ConversationModel(String name, ArrayList<Message> messages, ArrayList<User> users, boolean groupChat, int id) {
        this.name = name;
        this.messages = messages;
        this.users = users;
        this.groupChat = groupChat;
        this.id = id;
    }
}
