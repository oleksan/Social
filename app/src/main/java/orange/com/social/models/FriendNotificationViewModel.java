package orange.com.social.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FriendNotificationViewModel {

    @SerializedName("Id")
    @Expose
    private int id;

    @SerializedName("DateOfCreate")
    @Expose
    private String dateOfCreate;

    @SerializedName("Reciver")
    @Expose
    private User reciver;

    @SerializedName("Sender")
    @Expose
    private User sender;

    @SerializedName("Description")
    @Expose
    private String description;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateOfCreate() {
        return dateOfCreate;
    }

    public void setDateOfCreate(String dateOfCreate) {
        this.dateOfCreate = dateOfCreate;
    }

    public User getReciver() {
        return reciver;
    }

    public void setReciver(User reciver) {
        this.reciver = reciver;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}


