package orange.com.social.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GroupConversationCreationParams {

    @SerializedName("ConversationName")
    @Expose
    private String conversationName;

    @SerializedName("ParticipantsIds")
    @Expose
    private List<String> participantsIds;

    public String getConversationName() {
        return conversationName;
    }

    public void setConversationName(String conversationName) {
        this.conversationName = conversationName;
    }

    public List<String> getParticipantsIds() {
        return participantsIds;
    }

    public void setParticipantsIds(List<String> participantsIds) {
        this.participantsIds = participantsIds;
    }

    public GroupConversationCreationParams(String conversationName, List<String> participantsIds) {
        this.conversationName = conversationName;
        this.participantsIds = participantsIds;
    }
}
