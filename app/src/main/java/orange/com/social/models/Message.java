package orange.com.social.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("DateOfSend")
    @Expose
    private String dateOfSend;

    @SerializedName("ConversationId")
    @Expose
    private int conversationId;

   /* @SerializedName("ConversationModel")
    @Expose
    private ConversationModel conversation;*/

    @SerializedName("SenderId")
    @Expose
    private String senderId;

//    @SerializedName("Sender")
//    @Expose
//    private User sender;

    @SerializedName("MessageText")
    @Expose
    private String messageText;

    @SerializedName("Id")
    @Expose
    private int id;

    public String getDateOfSend() {
        return dateOfSend;
    }

    public void setDateOfSend(String dateOfSend) {
        this.dateOfSend = dateOfSend;
    }

    public int getConversationId() {
        return conversationId;
    }

    public void setConversationId(int conversationId) {
        this.conversationId = conversationId;
    }

    /*public ConversationModel getConversation() {
        return conversation;
    }

    public void setConversation(ConversationModel conversation) {
        this.conversation = conversation;
    }*/

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    /*public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }*/

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Message(int conversationId, String messageText) {
        this.conversationId = conversationId;
        this.messageText = messageText;
    }

}
