package orange.com.social.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import orange.com.social.api.App;
import orange.com.social.R;
import orange.com.social.models.User;
import orange.com.social.adapters.RVAdapterFollows;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static orange.com.social.activities.MainActivity.token;

public class TabFragmentFollows extends Fragment {

    private List<User> follows;
    RVAdapterFollows adapter;
    RecyclerView rv;
    private View v;
    private int SKIP = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.tab_fragment_follows, container, false);
        rv = (RecyclerView) v.findViewById(R.id.recycler);
        follows = new ArrayList<>();
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.addOnScrollListener(scrollListener);
        adapter = new RVAdapterFollows(follows);
        rv.setAdapter(adapter);
        getRequestAllFollows(SKIP);
        return v;
    }

    private void noFollows() {
        if (follows.size() == 0) {
            RelativeLayout relativeLayout = (RelativeLayout) v.findViewById(R.id.relativeLayout);
            LinearLayoutCompat.LayoutParams tvParams = new LinearLayoutCompat.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT,
                    LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
            TextView tvNoFriends = new TextView(getActivity());
            tvNoFriends.setText("You have not follows");
            tvNoFriends.setLayoutParams(tvParams);
            relativeLayout.addView(tvNoFriends);
        }
    }

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() { // catch end of list and load next 20 users
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisibleItems = layoutManager.findFirstCompletelyVisibleItemPosition();

            if (pastVisibleItems + visibleItemCount >= totalItemCount && follows.size() >= SKIP + 20) {
                Toast.makeText(getActivity(), "End of list", Toast.LENGTH_SHORT).show();
                SKIP = SKIP + 20;
                getRequestAllFollows(SKIP);
            }
        }
    };

    private void getRequestAllFollows(int skip) {

        App.getOrangeApi().getAllFollows("bearer " + token, "application/javascript", 20, skip)
                .enqueue(new Callback<List<User>>() {
                    @Override
                    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                        if (response.isSuccessful()) {
                            follows.addAll(response.body());
                            rv.getAdapter().notifyDataSetChanged();
                            if (response.body().isEmpty()) {
                                noFollows();
                            }
                        } else {
                            noFollows();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<User>> call, Throwable t) {
                        Log.d("Error", t.getMessage());
                    }
                });
    }

}