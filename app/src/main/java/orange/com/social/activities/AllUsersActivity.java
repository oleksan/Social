package orange.com.social.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import orange.com.social.api.App;

import orange.com.social.models.GroupConversationCreationParams;
import orange.com.social.R;
import orange.com.social.models.User;

import orange.com.social.adapters.RVAdapterUsersForConversation;
import orange.com.social.adapters.RVAdapterAllUsers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static orange.com.social.activities.MainActivity.token;

public class AllUsersActivity extends AppCompatActivity {

    private List<User> allUsers;

    RecyclerView rv;
    private List<String> participantsIds;
    private String conversationName;
    private boolean usersForConversation;
    private RVAdapterUsersForConversation adapterForConversation;
    private RVAdapterAllUsers adapterAllUsers;
    private int SKIP = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_users);
        rv = (RecyclerView) findViewById(R.id.recycler);
        allUsers = new ArrayList<User>();
        LinearLayoutManager llm = new LinearLayoutManager(AllUsersActivity.this);
        rv.setLayoutManager(llm);
        rv.addOnScrollListener(scrollListener);//
        Intent intentForProfile = getIntent();
        usersForConversation = intentForProfile.getBooleanExtra("isUsersForConversation", false); // for add users to conversation
        conversationName = intentForProfile.getStringExtra("conversation name"); // from dialog window
        if (usersForConversation) {
            adapterForConversation = new RVAdapterUsersForConversation(allUsers);
            rv.setAdapter(adapterForConversation);
        } else {
            adapterAllUsers = new RVAdapterAllUsers(allUsers);
            rv.setAdapter(adapterAllUsers);
        }
        getRequestAllUsers(SKIP);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_all_users_for_conversation, menu);
        if (!usersForConversation) {
            menu.findItem(R.id.acept_users_for_conversation).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) { // дія кнопки, яка створює бесіду з доданих юзерів
        switch (item.getItemId()) {
            case R.id.acept_users_for_conversation:
                participantsIds = adapterForConversation.acceptUsersId();
                if (participantsIds.size() > 1 && participantsIds.size() < 16) { // обмеження кылькості юзерів у груповій бесіді
                    postCreateGroupConversation();
                    Intent intentConversation = new Intent(AllUsersActivity.this, ConversationsActivity.class);
                    startActivity(intentConversation);
                } else {
                    Toast.makeText(AllUsersActivity.this, "Choose from 2 to 15 people to create a group", Toast.LENGTH_SHORT).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() { // catch end of list and load next 20 users
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisibleItems = layoutManager.findFirstCompletelyVisibleItemPosition();

            if (pastVisibleItems + visibleItemCount >= totalItemCount && allUsers.size() >= SKIP + 20) {
                SKIP = SKIP + 20;
                getRequestAllUsers(SKIP);
            }
        }
    };

    private void noUsers() {
        if (allUsers.size() == 0) {
            RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
            LinearLayoutCompat.LayoutParams tvParams = new LinearLayoutCompat.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT,
                    LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
            TextView tvNoFriends = new TextView(AllUsersActivity.this);
            tvNoFriends.setText("You are alone in the social");
            tvNoFriends.setLayoutParams(tvParams);
            relativeLayout.addView(tvNoFriends);
        }
    }

    private void getRequestAllUsers(int skip) {

        App.getOrangeApi().getAllUsers("bearer " + token, "application/javascript", 20, skip)
                .enqueue(new Callback<List<User>>() {
                    @Override
                    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                        if (response.isSuccessful()) {

                            allUsers.addAll(response.body());
                            rv.getAdapter().notifyDataSetChanged();
                        } else {
                            noUsers();

                        }
                    }

                    @Override
                    public void onFailure(Call<List<User>> call, Throwable t) {
                        Log.d("Error", t.getMessage());
                    }
                });
    }

    private void postCreateGroupConversation() {

        GroupConversationCreationParams groupConversationCreationParams =
                new GroupConversationCreationParams(conversationName, participantsIds);

        Call<Void> call = App.getOrangeApi().createGroupConversation("bearer " + token,
                "application/json",
                groupConversationCreationParams);

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(AllUsersActivity.this, "Group conversation '"
                            + conversationName + "' was created", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AllUsersActivity.this, "Error on create", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

}
