package orange.com.social.activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


import java.util.ArrayList;
import java.util.List;

import orange.com.social.api.App;
import orange.com.social.R;
import orange.com.social.adapters.RVAdapterMessanger;
import orange.com.social.models.ConversationModel;
import orange.com.social.models.Message;
import orange.com.social.services.SignalRService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static orange.com.social.activities.MainActivity.token;

public class MessangerActivity extends AppCompatActivity {

    private final Context mContext = this;
    private SignalRService mService;
    private boolean mBound = false;
    private int intentId;

    static List<Message> messages;
    private static RVAdapterMessanger adapterMessanger;
    static RecyclerView rv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messanger);


        Intent intent = new Intent();
        intent.setClass(mContext, SignalRService.class);

        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

        Intent in = getIntent();
        intentId = in.getIntExtra("conversation id", 0);// *intentId maybe = 0 from RVAdapterConversations
        String intentName = in.getStringExtra("conversation name");
        rv = (RecyclerView) findViewById(R.id.recycler_message);
        messages = new ArrayList<Message>();
        LinearLayoutManager llm = new LinearLayoutManager(MessangerActivity.this);
        rv.setLayoutManager(llm);
        adapterMessanger = new RVAdapterMessanger(messages);
        rv.setAdapter(adapterMessanger);
        getRequestConversation();
        setTitle(intentName);

    }

    @Override
    protected void onStop() {
        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_messanger, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_logout) {
            ChatActivity.postLogout();
            Intent intentMain = new Intent(MessangerActivity.this, MainActivity.class);
            startActivity(intentMain);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void sendMessage(View view) {
        if (mBound) {
            // Call a method from the SignalRService.
            // However, if this call were something that might hang, then this request should
            // occur in a separate thread to avoid slowing down the activity performance.
            EditText editText = (EditText) findViewById(R.id.edit_message);
            //EditText editText_Receiver = (EditText) findViewById(R.id.edit_receiver);
            if (editText != null && editText.getText().length() > 0) {
                // String receiver = editText_Receiver.getText().toString();
                String message = editText.getText().toString();

                Message newMessage = new Message(intentId, message);
                postAddMessage(newMessage);
                editText.setText("");
            }
        }
    }

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private final ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to SignalRService, cast the IBinder and get SignalRService instance
            SignalRService.LocalBinder binder = (SignalRService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


    public static void updateNewMessage(Message message) {
        adapterMessanger.addNewMessage(adapterMessanger.getItemCount(), message);
        rv.smoothScrollToPosition(rv.getAdapter().getItemCount()); //  scrolling to last mesage in recyclerView
        beepForNewMessage();
    }

    private void updateNewMessageMy(Message msg) {
        adapterMessanger.addNewMessageMy(adapterMessanger.getItemCount(), msg);
        rv.smoothScrollToPosition(rv.getAdapter().getItemCount());
    }

    private static void beepForNewMessage() {
        ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
        toneGen1.startTone(ToneGenerator.TONE_CDMA_ANSWER, 150);
    }

    private void getRequestConversation() {

        Call<ConversationModel> call = App.getOrangeApi().getConversation("bearer " + token, "application/javascript",
                intentId);

        call.enqueue(new Callback<ConversationModel>() {
            @Override
            public void onResponse(Call<ConversationModel> call, Response<ConversationModel> response) {
                if (response.isSuccessful()) {
                    messages.addAll(response.body().getMessages());
                    rv.getAdapter().notifyDataSetChanged();
                    rv.smoothScrollToPosition(rv.getAdapter().getItemCount());
                }
            }

            @Override
            public void onFailure(Call<ConversationModel> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    private void postAddMessage(Message newMessage) {
        Call<Message> call = App.getOrangeApi().addMessage("bearer " + token,
                "application/json",
                newMessage);
        call.enqueue(new Callback<Message>() {
            @Override
            public void onResponse(Call<Message> call, Response<Message> response) {
                if (response.isSuccessful()) {
                    updateNewMessageMy(response.body());
                }

            }

            @Override
            public void onFailure(Call<Message> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

}
