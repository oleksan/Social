package orange.com.social.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import orange.com.social.api.App;
import orange.com.social.R;
import orange.com.social.adapters.RVAdapterConversations;
import orange.com.social.models.ConversationModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static orange.com.social.activities.MainActivity.token;

public class ConversationsActivity extends AppCompatActivity {

    private RecyclerView rv;
    private RVAdapterConversations adapter;
    private List<ConversationModel> allConversations;
    private int SKIP = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_con);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createConversationDialog();
            }
        });

        rv = (RecyclerView) findViewById(R.id.recyclerConv);
        allConversations = new ArrayList<ConversationModel>();
        LinearLayoutManager llm = new LinearLayoutManager(ConversationsActivity.this);
        rv.setLayoutManager(llm);
        rv.addOnScrollListener(scrollListener);
        adapter = new RVAdapterConversations(allConversations);
        rv.setAdapter(adapter);
        getRequestAllConversations(SKIP);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_conversation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_logout) {
            ChatActivity.postLogout();
            Intent intentMain = new Intent(ConversationsActivity.this, MainActivity.class);
            startActivity(intentMain);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void noConversations() {
        if (allConversations.size() == 0) {
            ConstraintLayout constraintLayout = (ConstraintLayout) findViewById(R.id.constraintLayout);
            LinearLayoutCompat.LayoutParams tvParams = new LinearLayoutCompat.LayoutParams(LinearLayoutCompat.LayoutParams.WRAP_CONTENT,
                    LinearLayoutCompat.LayoutParams.WRAP_CONTENT);
            TextView tvNoConversations = new TextView(ConversationsActivity.this);
            tvNoConversations.setText("You have not conversations");
            tvNoConversations.setLayoutParams(tvParams);
            constraintLayout.addView(tvNoConversations);
        }
    }

    private RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() { // catch end of list and load next 20 users
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int pastVisibleItems = layoutManager.findFirstCompletelyVisibleItemPosition();

            if (pastVisibleItems + visibleItemCount >= totalItemCount && allConversations.size() >= SKIP + 20) {
                Toast.makeText(ConversationsActivity.this, "End of list", Toast.LENGTH_SHORT).show();
                SKIP = SKIP + 20;
                getRequestAllConversations(SKIP);
            }
        }
    };

    private void getRequestAllConversations(int skip) {
        App.getOrangeApi().getAllConversations("bearer " + token, "application/javascript", 20, skip)
                .enqueue(new Callback<List<ConversationModel>>() {
                    @Override
                    public void onResponse(Call<List<ConversationModel>> call, Response<List<ConversationModel>> response) {
                        if (response.body() != null) {
                            allConversations.addAll(response.body());
                            rv.getAdapter().notifyDataSetChanged();
                            if (response.body().isEmpty()) {
                                noConversations();
                            }
                        } else {
                            noConversations();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<ConversationModel>> call, Throwable t) {
                    Log.d("Error", t.getMessage());
                    }
                });
    }

    private void createConversationDialog() {

        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Create conversation");
        alert.setMessage("Enter conversation name:");
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String conversationName = input.getText().toString();
                Intent intentAllUserActivity = new Intent(rv.getContext(), AllUsersActivity.class);
                intentAllUserActivity.putExtra("isUsersForConversation", true);
                intentAllUserActivity.putExtra("conversation name", conversationName);
                startActivity(intentAllUserActivity);

            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });

        alert.show();
    }

}
