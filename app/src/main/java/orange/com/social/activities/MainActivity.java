package orange.com.social.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import orange.com.social.api.App;
import orange.com.social.R;
import orange.com.social.models.UserTokenModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnLogin;
    private EditText etName;
    private EditText etPassword;

    private String sharedEmail;
    static String userName;
    public static String token;

    public static final String PREFS_NAME = "LoginData";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        Button btnReg = (Button) findViewById(R.id.btnReg);
        etName = (EditText) findViewById(R.id.etName);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnReg.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        sharedEmail = settings.getString("email", "");
        //String sharedToken = settings.getString("token", ""); // token = ""
        String sharedPassword = settings.getString("password", ""); // password = ""
        etName.setText(sharedEmail);
        etPassword.setText(sharedPassword);
        //token = sharedToken;
        //sharedUserName = settings.getString("userName", "");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                if (isLoginValid()) {
                    postLogin();
                    btnLogin.setEnabled(false);
                }
                break;
            case R.id.btnReg:
                Intent intentRegistration = new Intent(this, RegistrationActivity.class);
                startActivity(intentRegistration);
                break;
        }
    }

    private void startChat() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("email", etName.getText().toString());
        //editor.putString("token", token);
        editor.putString("password", etPassword.getText().toString());
        editor.putString("userName", userName);
        editor.commit();

        Intent intentChat = new Intent(this, ChatActivity.class);
        startActivity(intentChat);
    }

    private void postLogin() {
        Call<UserTokenModel> call = App.getOrangeApi().login("password", etPassword.getText().toString(), etName.getText().toString());

        call.enqueue(new Callback<UserTokenModel>() {
            @Override
            public void onResponse(Call<UserTokenModel> call, Response<UserTokenModel> response) {
                if (response.isSuccessful()) {
                    token = response.body().getAccess_token();
                    userName = response.body().getUserName();
                    btnLogin.setEnabled(true);
                    startChat();
                } else {
                    Toast.makeText(MainActivity.this, "Login failed ! ", Toast.LENGTH_SHORT).show();
                    btnLogin.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<UserTokenModel> call, Throwable t) {
                btnLogin.setEnabled(true);
                Log.d("Error", t.getMessage());
            }
        });

    }

    //check authorization
    private boolean isLoginValid() {
        return isNotEmptyFields() &&
                checkEmailLoginWithRegExp(etName.getText().toString());
    }

    private boolean checkEmailLoginWithRegExp(String emailText) {
        Pattern p = Pattern.compile(getString(R.string.email_reg_exp));
        Matcher m = p.matcher(emailText);
        if (!(m.matches())) {
            Toast.makeText(this, "Not correct email format !", Toast.LENGTH_SHORT).show();
        }
        return m.matches();
    }

    private boolean isNotEmptyFields() {
        if (etName.getText().toString().equals("") ||
                etPassword.getText().toString().equals("")) {
            Toast.makeText(this, "Feilds 'Email' or 'Password' can't be empty !", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

}