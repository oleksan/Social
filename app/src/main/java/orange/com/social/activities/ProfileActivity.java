package orange.com.social.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import orange.com.social.R;

public class ProfileActivity extends AppCompatActivity {

    private ImageView imageFoto;
    private TextView tvFirsName;
    private TextView tvLastName;
    private TextView tvBirthdayProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        imageFoto = (ImageView) findViewById(R.id.imageFoto);
        tvFirsName = (TextView) findViewById(R.id.tvFirsName);
        tvLastName = (TextView) findViewById(R.id.tvLastName);
        tvBirthdayProfile = (TextView) findViewById(R.id.tvBirthdayProfile);

        Intent intentForProfile = getIntent();
        boolean myProfile = intentForProfile.getBooleanExtra("OnlyMyProfile", false); // for only My profile
        if (myProfile) {
            tvFirsName.setText(intentForProfile.getStringExtra("MyFirstName"));
            tvLastName.setText(intentForProfile.getStringExtra("MyLastName"));

            tvBirthdayProfile.setText(intentForProfile.getStringExtra("MyDateOfBirth").substring(0, 10));
            if (intentForProfile.getStringExtra("MyGender").equals("male")) {
                imageFoto.setImageResource(R.drawable.ic_masculine);
            } else if (intentForProfile.getStringExtra("MyGender").equals("female")) {
                imageFoto.setImageResource(R.drawable.ic_femenine);
            }
        } else {
            tvFirsName.setText(intentForProfile.getStringExtra("friend first name"));
            tvLastName.setText(intentForProfile.getStringExtra("friend last name"));
            tvBirthdayProfile.setText(intentForProfile.getStringExtra("friend date of birth").substring(0, 10));
            if (intentForProfile.getStringExtra("friend gender").equals("male")) {
                imageFoto.setImageResource(R.drawable.ic_masculine);
            } else if (intentForProfile.getStringExtra("friend gender").equals("female")) {
                imageFoto.setImageResource(R.drawable.ic_femenine);
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_logout) {
            ChatActivity.postLogout();
            Intent intentMain = new Intent(ProfileActivity.this, MainActivity.class);
            startActivity(intentMain);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
