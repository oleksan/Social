package orange.com.social.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import orange.com.social.api.App;
import orange.com.social.R;
import orange.com.social.models.UserRegistrationProfileModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etRegPassword;
    private EditText etConfirmPassword;
    private EditText etEmail;
    private EditText etFirstName;
    private EditText etLastName;
    private EditText etDateOfBirth;
    private Spinner spinnerGender;
    private String[] dataSpinner = {"Male", "Female"};

    private DatePickerDialog.OnDateSetListener mDateSetListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        TextView tvRegPassword = (TextView) findViewById(R.id.tvRegPassword);
        TextView tvEmail = (TextView) findViewById(R.id.tvEmail);
        TextView tvFirstName = (TextView) findViewById(R.id.tvFirstName);
        TextView tvLastName = (TextView) findViewById(R.id.tvLastName);
        TextView tvGender = (TextView) findViewById(R.id.tvGender);
        TextView tvDateOfBirthday = (TextView) findViewById(R.id.tvDateOfBirth);
        etRegPassword = (EditText) findViewById(R.id.etRegPassword);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etFirstName = (EditText) findViewById(R.id.etFirstName);
        etLastName = (EditText) findViewById(R.id.etLastName);
        etDateOfBirth = (EditText) findViewById(R.id.etDateOfBirth);
        Button btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, dataSpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender = (Spinner) findViewById(R.id.spinnerGender);
        spinnerGender.setAdapter(adapter);
        spinnerGender.setSelection(0);
        spinnerGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        etDateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dateDialog = new DatePickerDialog(RegistrationActivity.this,
                        mDateSetListener, year, month, day);
                dateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dateDialog.setTitle("Date of Birthday");
                dateDialog.show();
            }
        });
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = year + "-" + month + "-" + dayOfMonth;
                etDateOfBirth.setText(date);
            }
        };
    }

    @Override
    public void onClick(View v) {
        if (isValidRegistration()) {
            UserRegistrationProfileModel userReg = new UserRegistrationProfileModel(
                    etEmail.getText().toString(),
                    etRegPassword.getText().toString(),
                    etConfirmPassword.getText().toString(),
                    etFirstName.getText().toString(),
                    etLastName.getText().toString(),
                    etDateOfBirth.getText().toString(),
                    spinnerGender.getSelectedItem().toString());
            postRegistration(userReg);
        }
    }

    private void startActivityMain() {
        Intent intentMain = new Intent(this, MainActivity.class);
        startActivity(intentMain);
    }

    private void postRegistration(UserRegistrationProfileModel userRegistrationProfile) {

        Call<Void> call = App.getOrangeApi().createAccount(userRegistrationProfile);

        call.enqueue(new Callback<Void>() {

            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(RegistrationActivity.this, "Registration succes", Toast.LENGTH_SHORT).show();
                    startActivityMain();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    //check registration values
    private boolean isValidRegistration() {
        if (checkEmailWithRegExp(etEmail.getText().toString()) &&
                minSymbolsPassword() &&
                comparePasswords() &&
                maxSymbolsInFirstName() &&
                maxSymbolsInLastName() &&
                isEmptyEditTexts()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkEmailWithRegExp(String emailText) {
        Pattern p = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
        Matcher m = p.matcher(emailText);
        if (!(m.matches())) {
            Toast.makeText(this, "Not correct email format !", Toast.LENGTH_SHORT).show();
        }
        return m.matches();
    }

    private boolean minSymbolsPassword() {
        if (etRegPassword.getText().toString().length() < 6) {
            Toast.makeText(this, "Password must have > 6 symbols !", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    private boolean comparePasswords() {
        if (etRegPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
            return true;
        } else {
            Toast.makeText(this, "The password and confirmation password do not match !", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private boolean maxSymbolsInFirstName() {
        if (etFirstName.getText().toString().length() > 30) {
            Toast.makeText(this, "FirstName must have < 30 symbols !", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    private boolean maxSymbolsInLastName() {
        if (etLastName.getText().toString().length() > 30) {
            Toast.makeText(this, "LastName must have < 30 symbols !", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    private boolean isEmptyEditTexts() {
        if (etEmail.getText().toString().equals("") ||
                etRegPassword.getText().toString().equals("") ||
                etConfirmPassword.getText().equals("") ||
                etFirstName.getText().toString().equals("") ||
                etLastName.getText().toString().equals("") ||
                etDateOfBirth.getText().toString().equals("")) {
            Toast.makeText(this, "All fields must be filled !", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }
}
