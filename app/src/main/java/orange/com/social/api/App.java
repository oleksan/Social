package orange.com.social.api;

import android.app.Application;

import orange.com.social.api.ApiEndPoint;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//class for one  Retrofit object for all requests,  in manifest Application   name = ".App"

public class App extends Application {
    private static ApiEndPoint orangeApi;
    private Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.3.2:49342/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        orangeApi = retrofit.create(ApiEndPoint.class);
    }

    public static ApiEndPoint getOrangeApi() {
        return orangeApi;
    }
}

