package orange.com.social.api;

import java.util.List;

import okhttp3.ResponseBody;
import orange.com.social.models.ConversationModel;
import orange.com.social.models.GroupConversationCreationParams;
import orange.com.social.models.Message;
import orange.com.social.models.User;
import orange.com.social.models.UserRegistrationProfileModel;
import orange.com.social.models.UserTokenModel;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiEndPoint {

    @FormUrlEncoded
    @POST("Token")
    Call<UserTokenModel> login(@Field("grant_type") String grantType,
                               @Field("password") String password,
                               @Field("username") String username);

    @GET("api/profile/profile")
    Call<User> getProfile(@Header("Authorization") String authToken,
                          @Header("Content-Type") String contentType);

    @POST("api/Account/Register")
    Call<Void> createAccount(@Body UserRegistrationProfileModel userRegistrationProfile);

    @POST("api/Account/Logout")
    Call<Void> logout(@Header("Authorization") String authToken,
                      @Header("Content-Type") String contentType);

    @GET("api/friends/allUsers")
//?limit={limit}&skip={skip}
    Call<List<User>> getAllUsers(@Header("Authorization") String authToken,
                                 @Header("Content-Type") String contentType,
                                 @Query("limit") int limit,
                                 @Query("skip") int skip);

    @GET("api/friends/getAllFriends")
//?limit={limit}&skip={skip}
    Call<List<User>> getAllFriends(@Header("Authorization") String authToken,
                                   @Header("Content-Type") String contentType,
                                   @Query("limit") int limit,
                                   @Query("skip") int skip);

    @GET("api/friends/getAllSubscribers")
//?limit={limit}&skip={skip}
    Call<List<User>> getAllSubscribers(@Header("Authorization") String authToken,
                                       @Header("Content-Type") String contentType,
                                       @Query("limit") int limit,
                                       @Query("skip") int skip);

    @GET("api/friends/getAllFollows")
//?limit={limit}&skip={skip}
    Call<List<User>> getAllFollows(@Header("Authorization") String authToken,
                                   @Header("Content-Type") String contentType,
                                   @Query("limit") int limit,
                                   @Query("skip") int skip);

    @POST("api/friends/subscribe")
// return User
    Call<Void> subscribe(@Header("Authorization") String authToken,
                         @Header("Content-Type") String contentType,
                         @Body String recieverId);

    @POST("api/friends/unsubscribe")
    Call<Void> unsubscribe(@Header("Authorization") String authToken,
                           @Header("Content-Type") String contentType,
                           @Body String reciverId);

    @POST("api/friends/confirmFriendRequest")
// return User
    Call<Void> confirmFriend(@Header("Authorization") String authToken,
                             @Header("Content-Type") String contentType,
                             @Body String subscriberId);

    @POST("api/friends/deleteFriend")
    Call<Void> deleteFriend(@Header("Authorization") String authToken,
                            @Header("Content-Type") String contentType,
                            @Body String friendId);


    @GET("api/conversation/getAllConversation")
        //?limit={limit}&skip={skip}
    Call<List<ConversationModel>> getAllConversations(@Header("Authorization") String authToken,
                                                      @Header("Content-Type") String contentType,
                                                      @Query("limit") int limit,
                                                      @Query("skip") int skip);

    //@FormUrlEncoded
    @POST("api/conversation/createGroupConversation")
    // response ConversationModel
    Call<Void> createGroupConversation(@Header("Authorization") String authToken,
                                       @Header("Content-Type") String contentType,
                                       @Body GroupConversationCreationParams groupConversationCreationParams);


    @POST("api/conversation/leaveConversation")
        //?conversationId={conversationId}
    Call<ResponseBody> leaveConversation(@Header("Authorization") String authToken,
                                         @Header("Content-Type") String contentType,
                                         @Body int conversationId);

    @GET("api/conversation/getConversation")
    Call<ConversationModel> getConversation(@Header("Authorization") String authToken,
                                            @Header("Content-Type") String contentType,
                                            @Query("conversationId") int conversationId);

    @POST("api/messages/addMessage")
        // response Message
    Call<Message> addMessage(@Header("Authorization") String authToken,
                             @Header("Content-Type") String contentType,
                             @Body Message message);

    @POST("api/messages/deleteMessage")
    Call<Void> deleteMessage(@Header("Authorization") String authToken,
                             @Header("Content-Type") String contentType,
                             @Body int messageId);
    
}
